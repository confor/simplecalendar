<?php
/**
 * Internationalization file for magic words.
 */

$magicWords = array();

$magicWords['en'] = [
	'calendar' => [ 0, 'calendar' ]
];

$magicWords['es'] = [
	'calendar' => [ 0, 'calendario' ]
];
