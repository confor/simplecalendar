<?php
class SimpleCalendar {

	/**
	 * Called when the extension is first loaded
	 */
	public static function onRegistration() {
		global $wgExtensionFunctions;
		$wgExtensionFunctions[] = __CLASS__ . '::setup';
	}

	public static function setup() {
		global $wgParser;
		$wgParser->setFunctionHook( 'calendar', [ self::class, 'render' ] );
	}

	/**
	 * Converts an array of values in form [0] => "name=value" into a real
	 * associative array in form [name] => value. If no = is provided,
	 * true is assumed like this: [name] => true
	 * https://www.mediawiki.org/w/index.php?title=Manual:Parser_functions&oldid=3320278#Named_parameters
	 *
	 * @param array string $options
	 * @return array $results
	 */
	private static function extractOptions( array $options ) {
		$results = array();

		foreach ( $options as $option ) {
			$pair = explode( '=', $option, 2 );
			if ( count( $pair ) === 2 ) {
				$name = trim( $pair[0] );
				$value = trim( $pair[1] );
				$results[$name] = $value;
			}

			if ( count( $pair ) === 1 ) {
				$name = trim( $pair[0] );
				$results[$name] = true;
			}
		}

		return $results;
	}

	/**
	 * Expands the "calendar" magic word to a table of all the individual month tables
	 */
	public static function render( $parser ) {
		$parser->disableCache(); // !
		$argv = self::extractOptions( array_slice(func_get_args(), 1) );

		// Set options to defaults or specified values
		$p = isset( $argv['prefix'] ) ? $argv['prefix']  : '';
		$s = isset( $argv['suffix'] ) ? $argv['suffix'] : '';
		$q = isset( $argv['query'] )  ? $argv['query'] . '&action=edit' : 'action=edit';
		$y = isset( $argv['year'] )   ? $argv['year']                   : date( 'Y' );

		// backwards compat hotfix
		if (isset($argv['mes']))
			$argv['month'] = $argv['mes'];

		// If a month is specified, return only that month's table
		if ( isset( $argv['month'] ) ) {
			$m = $argv['month'];
			$table = self::renderMonth( strftime( '%m', strtotime( "$y-$m-01" ) ), $y, $p, $s, $q );
		}

		// Otherwise start month at 1 and build the main container table
		else {
			$m = 1;
			$table = "<table><tr><th colspan=\"4\">$y</th></tr><tr>";
			for ( $rows = 3; $rows--; $table .= "</tr><tr>" ) {
				for ( $cols = 0; $cols < 4; $cols++ ) {
					$table .= "<td>\n" . self::renderMonth( $m++ , $y, $p, $s, $q ) . "\n</td>";
				}
			}
			$table .= "</tr></table>\n";
		}

		return array( $table, 'isHTML' => true, 'noparse' => true );
	}

	/**
	 * Return a calendar table of the passed month and year
	 */
	private static function renderMonth( $m, $y, $prefix, $suffix, $query ) {
		if ( !$d = date( 'w', $ts = mktime( 0, 0, 0, $m, 1, $y ) ) ) {
			$d = 7;
		}
		$month = self::localized_month($m);
		$days = self::localized_week();

		$table = "\n<table class=\"calendar month\">\n\t<tr><th colspan=\"7\">$month</th></tr>\n";
		$table .= "\t<tr><td>" . implode( '</td><td>', $days ) . "</td></tr>\n";
		$table .= "\t<tr>\n";
		if ( $d > 1 ) {
			$table .= "\t\t" . str_repeat( "<td>&nbsp;</td>", $d - 1 ) . "\n";
		}
		for ( $i = $day = $d; $day < 32; $i++ ) {
			$day = $i - $d + 1;
			if ( $day < 29 or checkdate( $m, $day, $y ) ) {
				if ( $i % 7 == 1 ) {
					$table .= "\n\t</tr>\n\t<tr>\n";
				}
				$ttext = $prefix . self::localized_date($m, $day) . $suffix;
				$title = Title::newFromText( $ttext );
				$class = '';
				if ( is_object( $title ) ) {
					if (!$title->exists())
						$class = ' class="new"';
					$url = $title->getLocalURL( $title->exists() ? '' : $query );
				} else {
					$url = htmlspecialchars($ttext);
				}
				$table .= "\t\t<td><a href=\"$url\"$class>$day</a></td>\n";
			}
		}
		$table .= "\n\t</tr>\n</table>";
		return $table;
	}

	static function localized_date($month, $day) {
		// mediawiki key names
		$keys = ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'];

		// example for `january-date`:
		// english: "January $day",
		// spanish: "$day de enero",
		// german: "$day Januar", etc
		return wfMessage( $keys[$month - 1] . '-date' )->numParams($day)->escaped();
	}

	static function localized_month($month) {
		// mediawiki key names
		// warning: `may_long` exists only in some languages
		$keys = ['january', 'february', 'march', 'april', 'may_long', 'june', 'july', 'august', 'september', 'october', 'november', 'december'];

		return wfMessage( $keys[$month - 1] )->escaped();
	}

	static function localized_week($short = true) {
		// key names, not actually used to display
		$weekdays = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];

		$localized = [];

		foreach ($weekdays as $weekday) {
			$temp = wfMessage( $weekday )->escaped();
			if ($short)
				$temp = substr(strtoupper($temp), 0, 1);

			$localized[] = $temp;
		}

		return $localized;
	}
}
